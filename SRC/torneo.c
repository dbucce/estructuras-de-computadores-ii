#ifndef TORNEO
#define TORNEO 
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../INCL/structs.c"
#include "../INCL/torneo.h"
#include "../INCL/pshare.h"
#include "../INCL/gshare.h"

void torneo(int s, int size, int gh, int ph){

    //convencion de contadores: 
    //SPP = 00
    //WPP = 01
    //WPG = 10
    //SPG = 11

    int bht_size = pow(2,s); //tamaño del arreglo 
    int mask =0; //máscara para tomar los últimos s bits del PC 

    //se hace la predicción con    GSHARE Y PSHARE 
   
        pshare(s, size, ph); //se guarda en res3
        res3 = res; 
        gshare(s,size,gh); //se guarda en res2
        res2 = res; 
        

    //arreglo de contadores

    int counter[bht_size];

    //inicializo contadores en SPP 

   for (int i=0; i<bht_size; i++){
       counter[i]=0b00;

   }

   //inicializo máscara con s bits 
    for (int i=0; i<s; i++){
        mask = mask |1<<i; //creo máscara 

    }



    //hacer meta prediccion 

    for (int i=0; i<size; i++){
        int pc_counter = in.pc[i] & mask;

        //hago prediccion segun el contador seleccionado
    

        if (counter[pc_counter]==0b00){ //SPP
            res4.predictions[i] = res3.predictions[i];
        }else if (counter[pc_counter]==0b01){ //WPP
            res4.predictions[i] = res3.predictions[i];
        } else if (counter[pc_counter]==0b10){ //WPG
            res4.predictions[i] = res2.predictions[i];
        } else if (counter[pc_counter]==0b11){ //SPG
            res4.predictions[i] = res2.predictions[i];
        }
      

        //revisar si fue correcta o no la prediccion

        if (res4.predictions[i] == in.outcome[i]){
            //correcta
            res4.correct[i] = true; 
            if (in.outcome[i]=='T'){
                res4.correct_taken++; 
                
            } else {
                res4.correct_nt++;
            }
        } else {
            //incorrecta 
            res4.correct[i]=false;  
            if (res4.predictions[i]=='T'){
                res4.incorrect_taken++; 
            } else {
                res4.incorrect_nt++;
            }

        }

        //actualizar meta predictor  
        
       if (res2.predictions[i]==in.outcome[i]){
           //gshare correcto 
           if (res3.predictions[i]!=in.outcome[i]){
               //pshare incorrecto 
                if (counter[pc_counter]==0b00){ //SPP -> WPP
                    counter[pc_counter]=0b01;
                }else if (counter[pc_counter]==0b01){ //WPP->WPG
                    counter[pc_counter]=0b10;
                } else if (counter[pc_counter]==0b10){ //WPG->SPG
                    counter[pc_counter]=0b11;
                } 
           }
       } else{
           //gshare incorrecto 
           if (res3.predictions[i]==in.outcome[i]){
               //pshare correcto 
                if (counter[pc_counter]==0b11){ //SPG->WPG
                    counter[pc_counter]=0b10;
                }else if (counter[pc_counter]==0b01){ //WPP->SPP
                    counter[pc_counter]=0b00;
                } else if (counter[pc_counter]==0b10){ //WPG->WPP
                    counter[pc_counter]=0b01;
                } 
           }

       }


    }//for 


    res = res4; 
}

#endif

