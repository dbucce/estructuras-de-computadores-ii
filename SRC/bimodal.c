#ifndef BIMODAL
#define BIMODAL 
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include "../INCL/structs.c"
#include "../INCL/bimodal.h"


void bimodal(int s, int size){
    
    //convencion de contadores: 
    //SN = 00
    //WN = 01
    //WT = 10
    //ST = 11
    
    
    //arreglo
    int bht_size = pow(2,s);
    int counter [bht_size];
    int mask =0; 
    int pc_counter=0; 

    //obtengo pc counter con los ultimo s bits de PC
    for (int i=0; i<s; i++){
        mask = mask |1<<i; //creo máscara 

    }
    

    //inicializo contadores en SN
    for (int i=0; i<bht_size; i++){
        counter[i] = 0b00;
    }
    
    //recorro resultados 

    for (int i=0; i<size; i++){
        pc_counter = in.pc[i] & mask;

        //hago prediccion segun el contador seleccionado

        if (counter[pc_counter]==0b00){ //SN
            res.predictions[i] = 'N';
        }else if (counter[pc_counter]==0b01){ //WN
            res.predictions[i] = 'N';
        } else if (counter[pc_counter]==0b10){ //WT
            res.predictions[i] = 'T';
        } else if (counter[pc_counter]==0b11){ //ST
            res.predictions[i] = 'T';
        }

        //revisar si fue correcta o no la prediccion

        if (res.predictions[i] == in.outcome[i]){
            //correcta
            res.correct[i] = true; 
            if (in.outcome[i]=='T'){
                res.correct_taken++; 
                
            } else{
                res.correct_nt++;
            }
        } else {
            //incorrecta 
            res.correct[i]=false;  
            if (res.predictions[i]=='T'){
                res.incorrect_taken++; 
            } else{
                res.incorrect_nt++;
            }

        }

        //actualizar contadores 

        if (in.outcome[i]=='T'){
            if (counter[pc_counter]==0b00){ //SN->WN
                counter[pc_counter]=0b01;
            }else if (counter[pc_counter]==0b01){ //WN->WT
                counter[pc_counter]=0b10;
            } else if (counter[pc_counter]==0b10){ //WT->ST
                counter[pc_counter]=0b11;
            } 

        }else{
            if (counter[pc_counter]==0b11){ //ST->WT
                counter[pc_counter]=0b10;
            }else if (counter[pc_counter]==0b01){ //WN->SN
                counter[pc_counter]=0b00;
            } else if (counter[pc_counter]==0b10){ //WT->WN
                counter[pc_counter]=0b01;
            } 

        }


    }//for 

    
    
}//bimodal

#endif