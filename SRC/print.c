#ifndef PRINT
#define PRINT 
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "../INCL/structs.c"
#include "../INCL/print.h"

//imprimo tabla de resultados 

void print(char type[], int bht_size, int gh, int ph, int branch, int size ){

    
    float percentage = (res.correct_nt + res.correct_taken); 
    percentage = percentage /size;
    percentage= percentage*100;
  

    printf("-----------------------------\nPrediction parameters:\n-----------------------------\nBranch prediction type:  %s\nBHT size (entries):  %d\nGlobal history register size:  %d\nPrivate history register size:  %d\n-----------------------------\nSimulation results:\n-----------------------------\nNumber of branch:  %d\nNumber of correct prediction of taken branches:  %d\nNumber of incorrect prediction of taken branches:  %d\nCorrect prediction of not taken branches:  %d\nIncorrect prediction of not taken branches:  %d\nPercentage of correct predictions:  %f\n-----------------------------\n",  type,bht_size,gh, ph, branch, res.correct_taken, res.incorrect_taken, res.correct_nt, res.incorrect_nt, percentage);

}

//imprimo los primeros 50 resultados 

void printTable(){
    printf("\nPC           Outcome   Prediction  Correct/Incorrect:\n");
   for (int i=0; i<50; i++){
       char c[] = "incorrect";
       if (res.correct[i]==true){
           c[0]=' ';
           c[1]=' ';
       }
       printf("%ld       %c          %c             %s\n", in.pc[i], in.outcome[i],  res.predictions[i], c);
   }
}


//guardar resultados en archivo de salida en directorio de RESULT


void saveTable(char path[]){

    FILE * file; 
    file = fopen(path, "w");
    
       
    char header[]="\n     PC      Outcome   Prediction  Correct/Incorrect:\n";
    fprintf(file, "%s",header);
   for (int i=0; i<5000; i++){
       char c[] = "incorrect";
       if (res.correct[i]==true){
           c[0]=' ';
           c[1]=' ';
       }
       fprintf(file, "%ld        %c          %c          %s\n", in.pc[i], in.outcome[i], res.predictions[i], c);
   }
   fclose(file);
}

#endif