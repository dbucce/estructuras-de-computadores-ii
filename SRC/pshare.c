#ifndef PSHARE
#define PSHARE 
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../INCL/structs.c"
#include "../INCL/pshare.h"




void pshare(int s,  int size,  int ph){

    //convencion de contadores: 
    //SN = 00
    //WN = 01
    //WT = 10
    //ST = 11

    int bht_size = pow(2,s); //tamaño del arreglo 
    int mask =0; //máscara para tomar los últimos s bits del PC 
    int mask2 =0; //máscara para tomar los gh bits de la historia 
   
   //arreglo de contadores
   
   int counter[bht_size];

//arreglo global de historias privadas
   int pht[bht_size]; 

   //inicializo contadores en SN 

   for (int i=0; i<bht_size; i++){
       counter[i]=0b00;

        //inicializo pht
        pht[i]=0;
    
   }

   //mascaras para tomar n bits de PC y ph bits de historia 
   
    for (int i=0; i<s; i++){
        mask = mask |1<<i; //creo máscara 

    }
     for (int i=0; i<ph; i++){
        mask2 = mask2 |1<<i; //creo máscara 
    }


    //hacer predicción

    for (int i=0; i<size; i++){
        int pc_counter = in.pc[i] & mask; //defino pc_counter como los ultimos s bits del PC 
        int historia = pht[pc_counter] & mask2; //tomo los ph bits de la historia privada 
       
        int index = (pc_counter ^ historia) & mask; //defino el index como el XOR entre el pc counter y el registro de historia 
        
       

        //predicción a partir del index 
        
        if (counter[index]==0b00){ //SN
            res.predictions[i] = 'N';
        }else if (counter[index]==0b01){ //WN
            res.predictions[i] = 'N';
        } else if (counter[index]==0b10){ //WT
            res.predictions[i] = 'T';
        } else if (counter[index]==0b11){ //ST
            res.predictions[i] = 'T';
        }

        //revisar si la predicción fue correcta 
         if (res.predictions[i] == in.outcome[i]){
            //correcta
            res.correct[i] = true; 
            if (in.outcome[i]=='T'){
                res.correct_taken++; 
                
            } else {
                res.correct_nt++;
            }
        } else {
            //incorrecta 
            res.correct[i]=false;  
            if (res.predictions[i]=='T'){
                res.incorrect_taken++; 
            } else {
                res.incorrect_nt++;
            }

        }

        //actualizar contadores y bits de historia 
        if (in.outcome[i]=='T'){
            pht[pc_counter] <<= 1;
            pht[pc_counter] |=1;
            
            if (counter[index]==0b00){ //SN->WN
                counter[index]=0b01;
            }else if (counter[index]==0b01){ //WN->WT
                counter[index]=0b10;
            } else if (counter[index]==0b10){ //WT->ST
                counter[index]=0b11;
            } 

        }else{
            pht[pc_counter]<<=1;
            
            if (counter[index]==0b11){ //ST->WT
                counter[index]=0b10;
            }else if (counter[index]==0b01){ //WN->SN
                counter[index]=0b00;
            } else if (counter[index]==0b10){ //WT->WN
                counter[index]=0b01;
            } 

        }


        
    }//for
    

 

}//pshare

#endif