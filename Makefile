
#######################################
# Variables
#######################################

# C compiler
CC=gcc

# Files
BASEDIR=.
INCLDIR=$(BASEDIR)/INCL
SRCDIR=$(BASEDIR)/SRC
BINDIR=$(BASEDIR)/BIN
DOCDIR=$(BASEDIR)/DOC
RESDIR=$(BASEDIR)/RESULT 

OUTFILE=branch

# Compiler options and flags
CFLAGS= -g -I $(INCLDIR) -o $(BINDIR)/$(OUTFILE)

# `Magick++-config --cxxflags --cppflags` -o readWriteImages readWriteImages.cpp `Magick++-config --ldflags --libs`
#DFLAGS= -I /usr/local/include/opencv4/ -L /usr/local/lib -l opencv_core -l opencv_highgui -l opencv_imgproc -lopencv_video -l opencv_imgcodecs
#g++ -std=c++11 main.cpp `pkg-config --libs --cflags opencv4` -o result
#######################################
# Targets
#######################################

	#@$(CC) $(CFLAGS) $(LFLAGS) main.cpp $(SRCDIR)/*.cpp

all: clean build run clean

build:
	@echo "Compilando:"
	$(CC) main.c $(SRCDIR)/*.c  $(CFLAGS) -lm
	@echo "\nListo"
clean:
	@rm -rf $(BINDIR)/*
	@rm -f *~
	@echo "Archivos limpiados"

run:
	@echo "Ejecutando"
	gunzip -c branch-trace-gcc.trace.gz | ./$(BINDIR)/branch -s 3 -bp 2 -gh 4 -ph 3 -o 0
	@echo "\nListo" 

test: 
	@echo "Compilando:"
	$(CC) main.c $(SRCDIR)/*.c  $(CFLAGS) -lm
	@echo "\nListo"
	@echo "Ejecutando prueba"
	gunzip -c branch-trace-gcc.trace.gz | ./$(BINDIR)/branch -s 3 -bp 0 -gh 4 -ph 3 -o 1
	gunzip -c branch-trace-gcc.trace.gz | ./$(BINDIR)/branch -s 4 -bp 2 -gh 3 -ph 3 -o 1
	gunzip -c branch-trace-gcc.trace.gz | ./$(BINDIR)/branch -s 5 -bp 1 -gh 3 -ph 2 -o 1
	gunzip -c branch-trace-gcc.trace.gz | ./$(BINDIR)/branch -s 3 -bp 3 -gh 4 -ph 3 -o 1
	@echo "\nListo"



.PHONY: all clean doc
