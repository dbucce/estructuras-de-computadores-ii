#ifndef MAIN 
#define MAIN

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "INCL/bimodal.h"
#include "INCL/gshare.h"
#include "INCL/data.h"
#include "INCL/print.h"
#include "INCL/pshare.h"
#include "INCL/torneo.h"
#include "INCL/structs.c"

  //Descripción: Programa principal en donde se leen los parámetros de entrada y se llaman a los diferentes métodos para leer los datos del archivo comprimido y llamar a los predictores 
  //Parámetros de entrada: argc corresponde a la cantidad de argumentos del programa y argv guarda cada argumento 
  //Parámetros de salida: devuelve un 0 si se ejecuta correctamente 

int main (int argc,char **argv)
{
    //Inicializar argumentos de entrada

    int s; 
    int bp;
    int gh;
    int ph; 
    int o; 

    //Obtener argumentos de entrada

    for (int i =0; i<argc; i++){
        if (strcmp(argv[i], "-s")==0){
            s = atoi(argv[i+1]);
        } 
        if (strcmp(argv[i], "-bp" )==0){
            bp = atoi(argv[i+1]);
        } 
        if (strcmp(argv[i], "-gh")==0){
            gh = atoi(argv[i+1]);
        } 
        if (strcmp(argv[i], "-ph")==0){
            ph = atoi(argv[i+1]);
        } 
        if (strcmp(argv[i], "-o")==0){
            o = atoi(argv[i+1]);
        } 
    }

  
    //Definimos el tamaño de los BHT según el valor de s
    
    int bht_size = pow(2,s);

   
    //Obtener datos del archivo de saltos

    int size = getData();
   

    //Llamar predictores 

    if (bp ==0){
        bimodal(s, size);
        print("Bimodal", bht_size, gh, ph, size, size);
        if (o==1){
            saveTable("RESULT/BIMODAL.txt");
        }
    } else if (bp==2){
        gshare(s, size, gh);
        print("GSHARE", bht_size, gh, ph, size, size);
        if (o==1){
            saveTable("RESULT/GSHARE.txt");
        }
    } else if (bp==1){
        pshare(s, size, ph);
        print("PSHARE", bht_size, gh, ph, size, size);
        if (o==1){
            saveTable("RESULT/PSHARE.txt");
        }
    } else if (bp==3){
        torneo(s,size, gh, ph);
        print("TORNEO", bht_size, gh, ph, size, size);
        if (o==1){
            saveTable("RESULT/TORNEO.txt");
        }

    } else {
        printf("Tipo de predictor incorrecto.\n");
        return 1; 
    }

    //printTable(); 

   return 0; 
}



#endif