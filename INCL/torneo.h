//Descripción: método que realiza el predictor por torneo a partir de los predictores de historia global y privada y un meta predictor para tomar alguno de los dos resultados, usando un arreglo de 2bc
//Parámetros de entrada: recibe el parámetro de entrada s que determina el tamaño del arreglo de contadores, size que es la cantidad total de datos y los parámetros de entrada gh y ph que indica los bits del registro de historia global y privada
//Parámetros de salida: no devuelve nada. Modifica directamente la estructura de result

void torneo(int s, int size, int gh, int ph);