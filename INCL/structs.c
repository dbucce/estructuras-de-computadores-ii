  #include <stdbool.h>

  //se definen estructuras para guardar datos de entrada y resultados para un máximo de 17 millones de datos 

  //Estructura de input se usa para guardar datos de entrada 
  //outcome guarda el caracter de T o N
  //pc guarda la dirección de PC 


  struct input{
       char outcome[17000000];
       long int pc[17000000] ;

   } ;


  //Estructura result se usa para guardar datos de las predicciones realizadas
  //predictions guarda el caracter de predicción T o N
  //correct guarda true o false dependiendo si la predicción fue correcta o no
  //contadores de predicciónes correctas e incorrectas de taken y not taken
   struct result {
        char predictions[17000000];
        bool correct[17000000]; 
        int correct_taken;
        int incorrect_taken;
        int correct_nt;
        int incorrect_nt;
        };

//Se crean estructuras 

//Cada resultado se guarda en la estructura res 
//Las estructuras res2, res3 y res4 se usan únicamente en el predictor por torneo 
struct result res; 
struct result res2; //gshare
struct result res3; //pshare
struct result res4; //torneo  
struct input in; 
