//Descripción: método para imprimir en consola la tabla de resumen con los resultados de las predicciones incluyendo cantidad de predicciones correctas e incorrectas, porcentaje de predicciones correctas, entre otros
//Parámetros de entrada: recibe un string type que indica el nombre del predictor ejecutado, bht_size el cual indica el tamaño de la BHT a partir del parámetro s, los parámetros de entrada gh y ph para los predictores de gshare y pshare, la cantidad de branches realizados y la cantidad de datos totales
//Parámetros de salida: no devuelve nada. Imprime en consola los resultados 

void print(char type[], int bht_size, int gh, int ph, int branch, int size );


//Descripción: método que imprime en consola los primeros 50 resultados de las predicciones del predictor utilizado. Usado para debuggear y hacer pruebas 
//Parámetros de entrada: no recibe nada. Obtiene los datos de las estructuras globales
//Parámetros de salida: no devuelve nada. Imprime en consola

void printTable();

//Descripción: método para guardar los primeros 5000 resultados de las predicciones en archivos de texto en el directorio RESULT para el predictor ejecutado
//Parámetros de entrada: no recibe nada. Obtiene los datos directamente de las estructuras globales
//Parámetros de salida: no devuelve nada. Guarda los datos creando los archivos de texto 


void saveTable();