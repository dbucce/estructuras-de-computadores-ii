//Descripción: método del predictor bimodal que lee los datos de entrada de la estructura de input y guarda en la estructura de result las predicciones realizadas a partir de contadores de 2bits. 
//Parámetros de entrada: recibe el parámetro s que se obtiene como argumento del programa el cual indica el tamaño de la BHT y el size que es la cantidad completa de datos 
//Parámetros de salida: función no devuelve nada. Modifica directamente la estructura de result 
void bimodal(int s, int size);