//Descripción: método que realiza el predictor con historia global usando un arreglo de 2bc y registro de historia de los últimos saltos y guarda las predicciones en la estructura de result 
//Parámetros de entrada: recibe el parámetro de entrada s que determina el tamaño del arreglo, size que es la cantidad total de datos y el parámetro de entrada gh que indica los bits del registro de historia global
//Parámetros de salida: no devuelve nada. Modifica directamente la estructura de result

void gshare(int s,  int size, int gh);