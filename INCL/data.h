//Descripción: método para procesar el archivo comprimido y obtener los datos de entrada para guardarlos en la estructura de input 
//Parámetros de entrada: no recibe nada. Modifica directamente la estructura de input 
//Parámetros de salida: devuelve la cantidad total de datos leídos del archivo comprimido 

int getData();