//Descripción: método que realiza el predictor con historia privada usando un arreglo de 2bc y un arreglo donde se guardan las historias privadas 
//Parámetros de entrada: recibe el parámetro de entrada s que determina el tamaño de la BHT y PHT, size que es la cantidad total de datos y el parámetro de entrada ph que indica los bits de los registros de historia privada
//Parámetros de salida: no devuelve nada. Modifica directamente la estructura de result

void pshare(int s,  int size,  int ph);